# Hardware documentation

## CPU

### Intel

* [Software Developer Manuals](http://www.intel.com/content/www/us/en/processors/architectures-software-developer-manuals.html)

### AMD

* [Developer Guides, Manuals & ISA Documents](http://developer.amd.com/resources/developer-guides-manuals/)

## Network

### Virtio

* [Specs](http://docs.oasis-open.org/virtio/virtio/v1.0/virtio-v1.0.pdf)
* [Legacy specs](http://ozlabs.org/~rusty/virtio-spec/virtio-0.9.5.pdf)

### Intel 8254x (E1000)

* [Software Developer's Manual](https://www.intel.com/content/dam/doc/manual/pci-pci-x-family-gbe-controllers-software-dev-manual.pdf)

### Realtek 816x

## Disk

### AHCI for Serial ATA

* [ATA/ATAPI Command Set](http://www.t13.org/documents/uploadeddocuments/docs2006/d1699r3f-ata8-acs.pdf) - from 2006 but still valid
* [ATA Command Set](http://www.t13.org/documents/UploadedDocuments/docs2016/di529r14-ATAATAPI_Command_Set_-_4.pdf) - from 2016
* [Official Intel Specs](http://www.intel.com/content/www/us/en/io/serial-ata/ahci.html) - latest version 1.3.1
* [OSDev.org AHCI arcticle](http://wiki.osdev.org/AHCI)
